﻿using System;
using System.Data;
using System.IO;

namespace AzureCallStreamChecker
{
    class Program
    {
        static object WriteLogWithConsoleLock = new object();
        static void Main(string[] args)
        {
            //This is a test
            database dbase = new database();
            dbase.errorFile = string.Format(@"c:\logs\AzureCallStreamChecker_{0}.log", DateTime.Now.ToString("yyyyMMdd"));
            WriteLogWithConsole("Connecting to Azure SQL DB.");
            DataTable dtReturn = dbase.Exec_MSSQL_SP_Get("dbo.sp_GetHeartbeat", GetAzureConnString(), new System.Data.SqlClient.SqlParameter("@system", "AzureCallStream - MainThread"));

            if (dtReturn != null && dtReturn.Rows.Count > 0)
            {
                WriteLogWithConsole("Data retreived - Rows returned: " + dtReturn.Rows.Count.ToString());
                if (Directory.Exists(@"\\10.0.0.89\c-drive\checkfiles\AzureCallStream\") == false)
                    Directory.CreateDirectory(@"\\10.0.0.89\c-drive\checkfiles\AzureCallStream\");
                WriteLogWithConsole("HBDT column: " + dtReturn.Rows[0]["hbdt"].ToString());
                WriteLogWithConsole("ErrorCount column: " + dtReturn.Rows[0]["errorCount"].ToString());
                try
                {
                    File.WriteAllText(@"\\10.0.0.89\c-drive\checkfiles\AzureCallStream\ACS.txt", string.Format("{0},{1}", DateTime.Parse(dtReturn.Rows[0]["hbdt"].ToString()).AddHours(-7).ToString("yyyyMMdd HH:mm:ss"), dtReturn.Rows[0]["errorCount"]));
                    WriteLogWithConsole("Finished writing file");
                }
                catch (Exception err)
                {
                    WriteLogWithConsole("Error writing file: " + err.ToString());
                }
               
            }
            else
            {
                WriteLogWithConsole("Unable to get results from SQL table.");
            }
            WriteLogWithConsole("Done");
        }

        static void WriteLogWithConsole(string text)
        {
            lock (WriteLogWithConsoleLock)
            {
                string consoleOut = string.Format("{0}: {1}", DateTime.Now, text);
                Console.WriteLine(consoleOut);
                string logFile = string.Format(@"c:\logs\AzureCallStreamChecker_{0}.log", DateTime.Now.ToString("yyyyMMdd"));
                string output = string.Format("{0}: {1}{2}", DateTime.Now, text, Environment.NewLine);
                try
                {
                    System.IO.File.AppendAllText(logFile, output);
                }
                catch (Exception)
                {

                }
            }

        }

        private static string GetAzureConnString()
        {
            return "Server=tcp:azsccsadb01.database.secure.windows.net,1433;Database=Remindco;User ID=azsc_AzureCallStream@azsccsadb01;Password=0$#!d3/4|X$9;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        }
    }
}
